class Trip {
  Trip({this.city, this.activities, this.date});

  String city;
  List<String> activities;
  DateTime date;
}

import 'package:flutter/material.dart';

import './widget/activity_list.dart';
import './widget/trip_activity_list.dart';
import './widget/trip_overview.dart';

import '../../models/city_model.dart';
import '../../models/trip_model.dart';
import '../../models/activity_model.dart';

import '../../datas/data.dart' as data;

class CityView extends StatefulWidget {
  CityView({this.city});

  static String routeName = '/city';
  final List<Activity> activities = data.activities;
  final City city;

  @override
  _CityViewState createState() => _CityViewState();
}

class _CityViewState extends State<CityView> with WidgetsBindingObserver {
  Trip myTrip;
  int index;

  @override
  void initState() {
    super.initState();
    index = 0;
    myTrip = Trip(activities: [], date: null, city: 'Paris');
  }

  List<Activity> get tripActivities {
    return widget.activities
        .where((activity) => myTrip.activities.contains(activity.id))
        .toList();
  }

  void setDate() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now().add(Duration(days: 1)),
      firstDate: DateTime.now(),
      lastDate: DateTime(2021),
    ).then((newDate) {
      if (newDate != null) {
        setState(() {
          myTrip.date = newDate;
        });
      }
    });
  }

  void switchIndex(newIndex) {
    setState(() {
      index = newIndex;
    });
  }

  void toggleActivity(String id) {
    setState(() {
      myTrip.activities.contains(id)
          ? myTrip.activities.remove(id)
          : myTrip.activities.add(id);
    });
  }

  void deleteTripActivity(String id) {
    setState(() {
      myTrip.activities.remove(id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.chevron_left,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          'Organize a trip',
          style: TextStyle(color: Colors.black),
        ),
        actions: [IconButton(icon: Icon(Icons.more_horiz), onPressed: null)],
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.grey,
        currentIndex: index,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            label: 'Discover',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'My activities',
          ),
        ],
        onTap: switchIndex,
      ),
      body: SafeArea(
        minimum: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
        child: Container(
          child: Column(
            children: [
              TripOverview(
                cityName: widget.city.name,
                trip: myTrip,
                date: setDate,
              ),
              Expanded(
                child: index == 0
                    ? ActivityList(
                        activities: widget.activities,
                        selectedActivities: myTrip.activities,
                        toggleActivity: toggleActivity,
                      )
                    : TripActivityList(
                        activities: tripActivities,
                        deleteTripActivity: deleteTripActivity,
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

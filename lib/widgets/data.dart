import '../models/activity_model.dart';
import 'package:flutter/cupertino.dart';
import '../datas/data.dart' as data;

class Data extends InheritedWidget {
  Data({Widget child}) : super(child: child);

  final List<Activity> activities = data.activities;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

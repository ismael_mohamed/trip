import 'package:end_app_project/views/city/city_view.dart';
import 'package:flutter/cupertino.dart';

import '../../../models/city_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CityCard extends StatelessWidget {
  CityCard({this.city});

  final City city;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            height: 100,
            child: Stack(
              fit: StackFit.expand,
              children: [
                Ink.image(
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.5), BlendMode.dstATop),
                  image: AssetImage(
                    city.image,
                  ),
                  fit: BoxFit.cover,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(5.0),
                    splashColor: Colors.black.withOpacity(0.5),
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        '/city',
                        arguments: city,
                      );
                    },
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      city.name,
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.w700),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

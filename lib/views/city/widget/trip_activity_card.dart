import 'dart:math';

import 'package:end_app_project/models/activity_model.dart';
import 'package:flutter/material.dart';

class TripActivityCard extends StatefulWidget {
  TripActivityCard({Key key, this.activity, this.deleteTripActivity})
      : super(key: key);

  final Activity activity;
  final Function deleteTripActivity;

  Color getColor() {
    const colors = [Colors.blue, Colors.red];
    return colors[Random().nextInt(2)];
  }

  @override
  _TripActivityCardState createState() => _TripActivityCardState();
}

class _TripActivityCardState extends State<TripActivityCard> {
  Color color;

  @override
  void initState() {
    color = widget.getColor();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var activity = widget.activity;
    var deleteTripActivity = widget.deleteTripActivity;
    return Card(
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: AssetImage(activity.image),
        ),
        title: Text(
          activity.name,
          style: Theme.of(context).textTheme.headline6,
        ),
        subtitle: Text(activity.city),
        trailing: IconButton(
          onPressed: () {
            deleteTripActivity(activity.id);
          },
          icon: Icon(
            Icons.delete,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}

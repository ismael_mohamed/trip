import '../../../models/activity_model.dart';
import 'package:flutter/material.dart';

class ActivityCard extends StatelessWidget {
  ActivityCard({this.activity, this.toggleActivity, this.isSelected});

  final Activity activity;
  final bool isSelected;
  final Function toggleActivity;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              width: double.infinity,
              height: 150,
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Ink.image(
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.5), BlendMode.dstATop),
                    image: AssetImage(
                      activity.image,
                    ),
                    fit: BoxFit.cover,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(5.0),
                      splashColor: Colors.black.withOpacity(0.5),
                      onTap: toggleActivity,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                        Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              isSelected
                                  ? Icon(
                                      Icons.check_box_rounded,
                                      color: Colors.white,
                                    )
                                  : Icon(
                                      Icons.check_box_outline_blank_rounded,
                                      color: Colors.white,
                                    )
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Flexible(
                              child: Text(
                                activity.name,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

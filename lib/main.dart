import 'package:end_app_project/views/error/404_not_found.dart';
import 'package:flutter/material.dart';

import './models/city_model.dart';

import './views/city/city_view.dart';
import './views/home/home_view.dart';

void main() {
  runApp(DymaTrip());
}

class DymaTrip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
      ),
      debugShowCheckedModeBanner: false,
      // home: HomeView(),
      // initialRoute: '/city',
      routes: {
        '/': (context) {
          return HomeView();
        },
      },
      // ignore: missing_return
      onGenerateRoute: (settings) {
        if (settings.name == CityView.routeName) {
          final City city = settings.arguments;
          return MaterialPageRoute(
            builder: (context) {
              return CityView(
                city: city,
              );
            },
          );
        }
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (context) {
            return NotFound();
          },
        );
      },
    );
  }
}

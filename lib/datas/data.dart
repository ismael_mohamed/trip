import '../models/activity_model.dart';

List<Activity> activities = [
  Activity(
    id: '1',
    name: 'Les machines',
    image: 'assets/images/activities/les_machines.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '2',
    name: 'Hangars à bananes',
    image: 'assets/images/activities/hangars_bananes.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '3',
    name: 'Château des ducs de Bretagne',
    image: 'assets/images/activities/chateau_ducs_bretagne.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '4',
    name: 'Cathédrale St-Pierre',
    image: 'assets/images/activities/cat_st_pierre.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '5',
    name: 'Lieu unique',
    image: 'assets/images/activities/lieu_unique.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '6',
    name: 'Place Graslin',
    image: 'assets/images/activities/place_graslin.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '7',
    name: 'La Tour LU',
    image: 'assets/images/activities/tour_lu.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '8',
    name: 'Galipy Orvault',
    image: 'assets/images/activities/galipy_orvault.jpg',
    city: 'Nantes',
  ),
  Activity(
    id: '9',
    name: 'Transfert & Co',
    image: 'assets/images/activities/transfert_and_co.jpg',
    city: 'Nantes',
  ),
];
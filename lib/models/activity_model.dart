class Activity {
  Activity({this.id, this.name, this.image, this.city});

  String id;
  String name;
  String image;
  String city;
}

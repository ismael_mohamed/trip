import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../models/trip_model.dart';

class TripOverview extends StatelessWidget {
  TripOverview({this.date, this.trip, this.cityName});

  final Function date;
  final Trip trip;
  final String cityName;

  double get amount {
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;
    var size = MediaQuery.of(context).size;

    return Container(
      width: orientation == Orientation.landscape ? size.width * 0.5 : double.infinity,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 15, bottom: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    cityName,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Container(
                  child: RichText(
                      text: TextSpan(
                          text: 'Total amount : ',
                          style: TextStyle(color: Colors.grey),
                          children: [
                        TextSpan(
                          text: '$amount \$',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 20,
                          ),
                        ),
                      ])),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: trip.date != null
                            ? Text(
                                DateFormat('dd/M/y').format(trip.date),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            : Text(
                                'No date selected',
                                style: TextStyle(color: Colors.grey),
                              ),
                      ),
                      RaisedButton(
                          color: Colors.grey[900],
                          elevation: 0,
                          child: Row(
                            children: [
                              Icon(
                                Icons.calendar_today,
                                color: Colors.white.withOpacity(0.5),
                                size: 20,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                'Select Date',
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          ),
                          onPressed: date)
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

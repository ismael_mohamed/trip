import 'package:flutter/material.dart';

import '../../models/city_model.dart';

import './widget/city_card.dart';

class HomeView extends StatefulWidget {
  static String routeName = '/';

  @override
  _HomeViewState createState() {
    return _HomeViewState();
  }
}

class _HomeViewState extends State<HomeView> {
  List cities = [
    City(name: 'Nantes', image: 'assets/images/cities/bg_nantes.jpg'),
    City(name: 'Lyon', image: 'assets/images/cities/bg_lyon.jpg'),
    City(name: 'Paris', image: 'assets/images/cities/bg_paris.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: Icon(
          Icons.home_filled,
          color: Colors.black,
        ),
        title: Text(
          'Dyma Trip',
          style: TextStyle(color: Colors.black),
        ),
        actions: [IconButton(icon: Icon(Icons.more_horiz), onPressed: null)],
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        minimum: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Container(
          child: Column(
            children: [
              ...cities.map((city) {
                return CityCard(
                  city: city,
                );
              })
            ],
          ),
        ),
      ),
    );
  }
}

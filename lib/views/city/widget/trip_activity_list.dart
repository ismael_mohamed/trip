import '../../../models/activity_model.dart';
import './trip_activity_card.dart';
import 'package:flutter/material.dart';

class TripActivityList extends StatelessWidget {
  TripActivityList({this.activities, this.deleteTripActivity});

  final List<Activity> activities;
  final Function deleteTripActivity;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: activities
            .map((activity) => TripActivityCard(
                  key: ValueKey(activity.id),
                  activity: activity,
                  deleteTripActivity: deleteTripActivity,
                ))
            .toList(),
      ),
    );
  }
}
